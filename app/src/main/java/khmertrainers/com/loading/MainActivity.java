package khmertrainers.com.loading;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import khmertrainers.com.loading.adapter.ViewPagerAdapter;
import khmertrainers.com.loading.fragment.FragmentOne;
import khmertrainers.com.loading.fragment.FragmentThree;
import khmertrainers.com.loading.fragment.FragmentTwo;

import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager;
    LinearLayout mDot;
    private int dotcount;
    private ImageView[] dots;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = findViewById(R.id.viewpager);
        mDot = findViewById(R.id.dot);


        //setup Viewpager
        setUpViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                    for(int i = 0; i < dotcount; i++){
                        dots[i].setImageDrawable(getDrawable(R.drawable.non));
                    }
                dots[position].setImageDrawable(getDrawable(R.drawable.active));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setUpViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentOne(),"FragmentOne");
        adapter.addFragment(new FragmentTwo(),"FragmentTwo");
        adapter.addFragment(new FragmentThree(),"FragmentThree");
        viewPager.setAdapter(adapter);

        dotcount = adapter.getCount();
        dots = new ImageView[dotcount];
        for (int i = 0; i <dotcount; i++){
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getDrawable(R.drawable.non));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8,0,8,0);
            mDot.addView(dots[i],params);
        }
        dots[0].setImageDrawable(getDrawable(R.drawable.active));
    }
}
